const tree = 
{
    name: "A",
    children: 
    [
      {
        name: "B",
        children: 
        [
          {
            name: "C",
            children: 
            [
              {
                name: "D",
                children: []
              },
              {
                name: "E",
                children: []
              }
            ]
          },
          {
            name: "F",
            children: []
          }
        ]
      },
      {
        name: "G",
        children: []
      }
    ]
  };

  let count=0;

  let isObject=inp =>
  {
    if(inp==null)
    {
        return false;
    }else if (typeof inp=='object')
    {
            return true;
    }
  }


  let countNodes=obj=>
  {
    for(let prop in obj)
    {
        if(prop=='name')count++;
        else if(isObject(obj[prop]))
        {
            countNodes(obj[prop]);
        }
    }
    return count;
  }
 countNodes(tree);
